{
  description = "A Nix-flake-based Node.js development environment";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  #
  # In case an older NodeJS version is needed than is provided by the current
  # nixpkgs branch, use the following websites to find a commit of nixpkgs
  # that has the version of NodeJS you need and that has been built by Hydra
  # (otherwise NodeJS would have to be compiled from source whenever the dev
  # environment is created):
  #
  #  https://lazamar.co.uk/nix-versions/?channel=nixpkgs-unstable&package=nodejs
  #  https://hydra.nixos.org/job/nixpkgs/trunk/nodejs.x86_64-linux/all
  #
  # Once you have found a fitting commit add this revision as an additional flake
  # input:
  #
  # inputs.nixpkgs_bc714f86.url = "github:NixOS/nixpkgs/bc714f86a515a558c2f595e1a090e882d08bd7ca";

  #
  # Add the import to the function arguments:
  #
  # outputs = { self, nixpkgs, nixpkgs_bc714f86 }:
  outputs =
    { self, nixpkgs }:
    let
      overlays = [
        (final: prev: rec {
          #
          # Add the appropriate overlay:
          #
          # nodejs = prev.nodejs-12_x;
          nodejs = prev.nodejs_22;
          pnpm = prev.pnpm.override { inherit nodejs; };
        })
      ];
      supportedSystems = [
        "x86_64-linux"
        "aarch64-linux"
        "x86_64-darwin"
        "aarch64-darwin"
      ];
      forEachSupportedSystem =
        f:
        nixpkgs.lib.genAttrs supportedSystems (
          system:
          f {
            pkgs = import nixpkgs { inherit overlays system; };
            #
            # Import nixpkgs from the additional input:
            #
            # pkgs_bc714f86 = import nixpkgs_bc714f86 { inherit system overlays; };
          }
        );
    in
    {
      #
      # Add the additional nixpkgs instance to the function arguments:
      #
      # devShells = forEachSupportedSystem ({ pkgs, pkgs_bc714f86 }: {
      devShells = forEachSupportedSystem (
        { pkgs }:
        {
          #
          # If you are using an old revision of nixpkgs you should make sure the dev
          # tooling (gcc, ...) matches the version used for node, otherwise compiling
          # certain libraries via npm install (for example librdkafka) results
          # in a version that cannot be executed in the dev shell (probably because
          # of too old version of libc++)
          #
          # default = pkgs.mkShell.override {
          #   stdenv = pkgs_bc714f86.stdenv;
          # } {
          default = pkgs.mkShell {
            #
            # Finally use the old nixpkgs instance to install the packages you need:
            #
            # packages = with pkgs_bc714f86; [ nodejs ];
            packages = with pkgs; [
              nodejs
              pnpm
            ];
          };
        }
      );
    };
}
