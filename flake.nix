{
  description = "Ready-made templates for easily creating flake-driven environments";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  outputs = { self, nixpkgs }:
    let
      overlays = [
        (final: prev:
          {
            format = prev.writeScriptBin "format" ''
              ${final.lib.getExe final.nixfmt-rfc-style} **/*.nix
            '';
            update = prev.writeScriptBin "update" ''
              for dir in `ls -d */`; do # Iterate through all the templates
                (
                  cd $dir
                  ${final.lib.getExe final.nix} flake update # Update flake.lock
                  ${final.lib.getExe final.nix} flake check  # Make sure things work after the update
                )
              done
            '';
          })
      ];
      supportedSystems = [ "x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ];
      forEachSupportedSystem = f: nixpkgs.lib.genAttrs supportedSystems (system: f {
        pkgs = import nixpkgs { inherit overlays system; };
      });
    in
    {
      devShells = forEachSupportedSystem ({ pkgs }: {
        default = pkgs.mkShell {
          packages = with pkgs; [ format update ];
        };
      });
    }

    //

    {
      templates = {
        java = {
          path = ./java;
          description = "Java development environment";
        };
        node = {
          path = ./node;
          description = "Node.js development environment";
        };
        python = {
          path = ./python;
          description = "Python development environment";
        };
        rust = {
          path = ./rust;
          description = "Rust development environment";
        };
      };
    };
}
