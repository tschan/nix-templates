{
  description = "A Nix-flake-based rust development environment";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
    rust-overlay.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs =
    {
      self,
      nixpkgs,
      rust-overlay,
    }:
    let
      overlays = [
        # Makes a `rust-bin` attribute available in Nixpkgs
        (import rust-overlay)
        (final: prev: {
          rustToolchain = prev.rust-bin.stable.latest.default.override {
            # make rust source files available for e.g. IDEs
            extensions = [ "rust-src" ];
          };
        })
      ];
      supportedSystems = [
        "x86_64-linux"
        "aarch64-linux"
        "x86_64-darwin"
        "aarch64-darwin"
      ];
      forEachSupportedSystem =
        f:
        nixpkgs.lib.genAttrs supportedSystems (
          system: f { pkgs = import nixpkgs { inherit overlays system; }; }
        );
    in
    {
      devShells = forEachSupportedSystem (
        { pkgs }:
        {
          default = pkgs.mkShell {
            packages =
              (with pkgs; [
                # The package provided by our custom overlay. Includes cargo, Clippy, cargo-fmt,
                # rustdoc, rustfmt, and other tools.
                rustToolchain
                cmake
                # used for debugging in IDEs
                lldb
                # add any additional library needed by the project, e.g.:
                # libopus
              ])
              ++ pkgs.lib.optionals pkgs.stdenv.isDarwin (with pkgs; [ libiconv ]);
          };
        }
      );
    };
}
