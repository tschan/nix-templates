{
  description = "A Nix-flake-based python development environment";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  outputs =
    { self, nixpkgs }:
    let
      overlays = [
        (final: prev: {
          python = prev.python311.withPackages (
            ps: with ps; [
              virtualenv
              pip
            ]
          );
          lib-path =
            with prev;
            lib.makeLibraryPath [
              libffi
              openssl
              stdenv.cc.cc
              # If necessary add additional libraries like:
              # libz
              # linuxPackages.nvidia_x11
            ];
        })
      ];
      supportedSystems = [
        "x86_64-linux"
        "aarch64-linux"
        "x86_64-darwin"
        "aarch64-darwin"
      ];
      forEachSupportedSystem =
        f:
        nixpkgs.lib.genAttrs supportedSystems (
          system: f { pkgs = import nixpkgs { inherit overlays system; }; }
        );
    in
    {
      devShells = forEachSupportedSystem (
        { pkgs }:
        {
          default = pkgs.mkShell {
            packages = with pkgs; [ python ];

            shellHook = ''
              # Allow the use of wheels.
              SOURCE_DATE_EPOCH=$(date +%s)

              # Augment the dynamic linker path
              export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${pkgs.lib-path}"

              # Setup the virtual environment if it doesn't already exist.
              VENV=.venv
              if test ! -d $VENV; then
                virtualenv $VENV
              fi
              source ./$VENV/bin/activate
              export PYTHONPATH=`pwd`/$VENV/${pkgs.python.sitePackages}/:$PYTHONPATH
            '';
          };
        }
      );
    };
}
